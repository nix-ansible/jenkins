# Jenkins
![coverage report](https://gitlab.com/nix-ansible/jenkins/badges/master/build.svg)  

# Ansible Role: Jenkins

Installs Jenkins CI on Ubuntu.

## Role Variables

Available variables are listed below, along with custom values (see `defaults/main.yml`):

The HTTP port for Jenkins' web interfakce.
```yml
    jenkins_http_port: 8080
```
The Jenkins default HTTP url.
```yml
    jenkins_default_url: "http://localhost"
```
Don't forget change jenkins admin password
```yml
    jenkins_admin_username: admin
    jenkins_admin_password: admin
```
Please don't forget paste value in variable. Follow instruction for [scm_private_key](https://help.github.com/articles/connecting-to-github-with-ssh/), and for [jenkins_slack_token](https://sqlnotesfromtheunderground.wordpress.com/2017/01/13/notify-slack-for-jenkins-builds/)

```yml
jenkins_extra_job_creds_config:
  - id: scm-key
    type: ssh-private-key
    user: jenkins
    description: ssh key to checkout github repos
    creds_key: "please_paste_scm_private_key"
  - id: slack-token
    type: secret-text
    description: slack api token
    secret: "please_paste_jenkins_slack_token"
```

## List of pre requirements and pip package what will be installed

    /ansible/roles/jenkins/vars/debian.yml

## Example Playbook

```yaml
---

- include_vars: "{{ ansible_os_family|lower }}.yml"

- name: Install jenkins prerequisite apt packages
  apt:
    name:  "{{ item }}"
    state: present
  with_items: "{{ jenkins_prereq_pkgs|union(jenkins_apt_pkgs_extra) }}"
  when: ansible_os_family|lower == 'debian'

- name: Install jenkins prerequisite yum packages
  yum:
    name:  "{{ item }}"
    state: present
  with_items: "{{ jenkins_prereq_pkgs|union(jenkins_yum_pkgs_extra) }}"
  when: ansible_os_family|lower == 'redhat'

- name: Install extra pip packages desired
  pip:
    name:  "{{ item }}"
    state: present
  with_items: "{{ jenkins_pip_pkgs_extra }}"

- name: Jenkins Install
  import_tasks: "{{ jenkins_role|default('master') }}.yml"
```

## License

BSD
